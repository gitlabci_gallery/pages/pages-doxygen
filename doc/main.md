Hello {#mainpage}
=====

Hello world in C++ to give an example of static web pages generation with Doxygen.

## Build the project

```sh
cd src/
g++ -I./ hello.cpp -o hello
./hello
```

## Make the documentation

```sh
cd doc/
doxygen
ls html/
```
