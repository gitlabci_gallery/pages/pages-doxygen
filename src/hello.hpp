/**
 *
 * @file hello.hpp
 * @copyright 2024 Inria. All rights reserved.
 * @brief Hello class
 *
 */
#ifndef HELLO_HPP
#define HELLO_HPP

#include <iostream>

/**
 * The Hello world class. Just to say 'Hello'.
 */
class HelloWorld
{
    public:
        /**
         * Prints Hello to the standard output.
         */
        void static PrintHelloWorld()
        {
            std::cout << "Hello World!\n";
        }
};

#endif // HELLO_HPP