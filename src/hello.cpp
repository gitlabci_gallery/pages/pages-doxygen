/**
 *
 * @file hello.cpp
 * @copyright 2024 Inria. All rights reserved.
 * @brief Hello program
 *
 */
#include <hello.hpp>

/**
 * @brief Hello program main
 * @param[in] argc number of arguments
 * @param[in] argv array of arguments
 * @return EXIT_SUCCESS if succeeds
 */
int main(int argc, char **argv) {
    HelloWorld::PrintHelloWorld();
    return EXIT_SUCCESS;
}