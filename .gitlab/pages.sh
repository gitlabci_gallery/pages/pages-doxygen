#!/bin/sh
set -ex

mkdir -p public/
cd doc/
doxygen
cp -r html/* ../public/
