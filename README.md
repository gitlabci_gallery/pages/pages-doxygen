This gitlab project gives an example of generating a doxygen documentation for a C++ application.

# The example application

The application **hello** is a C++ program that prints *Hello World!* to the standard output.
The source code is located in [src/](./src/) and can be built using a C++ compiler as follows:

```sh
cd src/
g++ -I./ hello.cpp -o hello
./hello
```

# The Doxygen documentation generation

The documentation can be generated with [Doxygen](https://www.doxygen.nl/manual/index.html) in the *doc* directory as follows:

```sh
cd doc/
doxygen
ls html/
```

The documentation is generated in the `html` sub-directory.

# Continuous integration

The gitlab-ci jobs are defined in the default [.gitlab-ci.yml](.gitlab-ci.yml) yaml script.
In this file we define two jobs, *test_linux* and *pages*, attached to two stages: *test* and *deploy*.
During the *test_linux* job the application is built and executed, see [.gitlab/test.sh](.gitlab/test.sh).
If the job succeeds then the second stage is triggered and the *pages* job is performed.
The job *pages* is the default name of the job which generates and publishes the [pages](https://docs.gitlab.com/ee/user/project/pages/) (static website) of the project in gitlab-ci.
During the *pages* job the html documentation is built with Doxygen and copied in the `public` directory which is added as job's artifact, see [.gitlab/pages.sh](.gitlab/pages.sh).

Note that we run the jobs in a Docker environment using the last [alpine Docker image](https://hub.docker.com/_/alpine).

Before launching the generation be sure you activated the CI/CD option in the `Settings/General/Visibility, project features, permissions` section of your gitlab project.
To use existing docker runners you must enable the [shared runners](https://ci.inria.fr/doc/page/gitlab/) in the `Settings/CI/CD/Runners` section.

To see the Doxygen documentation open the `Deploy/Pages` section, you will find the url, which is https://gitlabci_gallery.gitlabpages.inria.fr/pages/pages-doxygen/ here.

## Badges

Notice also the [badges](https://docs.gitlab.com/ee/user/project/badges.html) that have been set on the [main page of the project](https://gitlab.inria.fr/gitlabci_gallery/pages/pages-doxygen), (cf. pipeline and master documentation badges).

To set up or modify badges, the user requires a `Maintainer` or an `Owner` role to access the settings of the project.

- On the left sidebar in the Gitlab interface, go to **Settings** → **General**.
- Expand **Badges**, click on `Add badge`, or on the edit button (symbolized as a pen) of an existing badge:
  - Name the badge
  - Set the gitlab-pages URL in `Link` (cf. **Deploy** → **Pages** on the left sidebar in the Gitlab interface)
  - `Badge image URL` was set with `https://img.shields.io/static/v1?label=Documentation&&message=master&color=orange`, cf. https://shields.io/ to build your own custom static or dynamic badges.